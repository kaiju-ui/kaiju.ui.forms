import WindowForm, {TransparentWindowFormComponent} from "WindowForm/WindowForm";
import WindowFormStore from "WindowForm/WindowFormStore";

export default WindowForm
export {WindowFormStore, TransparentWindowFormComponent};