import StringStore from "FieldGroups/String";
import NumberStore, {DecimalStore} from "FieldGroups/Number";
import BooleanStore from "FieldGroups/Boolean";
import DatetimeStore from "FieldGroups/Datetime";
import SelectStore, {MultiSelectStore, TagSelectStore} from "FieldGroups/Select";
import JsonStore from "FieldGroups/JsonObject";
import FieldStore from "FieldGroups/store";
import valueStore from "FieldGroups/valueStore";

const FieldsKindMap = {
    string: StringStore,
    decimal: DecimalStore,
    integer: NumberStore,
    number: NumberStore,
    boolean: BooleanStore,
    datetime: DatetimeStore,
    date: DatetimeStore,
    select: SelectStore,
    multiselect: MultiSelectStore,
    multi_select: MultiSelectStore,
    tag: TagSelectStore,
    json_object: JsonStore
};

const FieldsKeyMap = {};

export {FieldsKindMap, FieldsKeyMap, FieldStore, valueStore};