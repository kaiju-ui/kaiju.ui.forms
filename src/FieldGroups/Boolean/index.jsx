import BooleanComponent from "FieldGroups/Boolean/component";
import {computed} from "mobx";
import FieldStore from "FieldGroups/store";


export default class BooleanStore extends FieldStore {
    Component = BooleanComponent;

    @computed get isChanged() {
        return !this.oldValue.equal(this.value)
    }
}
