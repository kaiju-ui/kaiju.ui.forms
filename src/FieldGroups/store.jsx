import {action, computed, observable} from "mobx";
import BaseValueStore from "FieldGroups/valueStore";
import {uuid4} from "@kaiju.ui/components/src/utils";
import equal from "deep-equal";

class FieldStore {
    // 1. Определение behaviorComponent
    // 2. Хранение valueComponent'а
    // 3. Хранение всех данных поле
    // 4. Изменение value поля
    // 5. Вызывает сравнения у behavior - старое и новое значение


    @observable value;
    @observable show = true;
    @observable oldValue;
    ValueStore = BaseValueStore;
    Component; // указывать у children

    constructor(conf, formStore) {
        this.uuid = uuid4();
        this.conf = conf;
        this.conf.extraParams = formStore.extraParams;
        this.prefix = formStore.prefix;
        this.formStore = formStore;
        this.init();
        this.dependence = this.conf.dependence;

        if (this.conf.isMulti) {
            this.dependenceCompareFunction = this.dependence?.condition == 'in_list' ? this.compareMultiSelectInList : this.compareMultiSelectAll
        } else {
            this.dependenceCompareFunction = this.compareSelect
        }

        let cachedValue = this.getValueFromCache();

        const value = (conf.value === null || conf.value === undefined) ? conf.default : conf.value;

        this.value = this.setValue(cachedValue !== undefined ? cachedValue : value);
        this.oldValue = this.setValue(value);
    }

    compareMultiSelectInList(value) {
        return this.dependence.value.some(v => v === value)
    }

    compareMultiSelectAll(value) {
        return equal(value, this.dependence.value)
    }

    compareSelect(value) {
        return this.dependence.value == value
    }

    reCreateValue() {
        const value = (this.conf.value === null || this.conf.value === undefined) ? this.conf.default : this.conf.value;

        let cachedValue = this.getValueFromCache();
        this.value = this.setValue(cachedValue !== undefined ? cachedValue : value);
    }

    setSaveValue() {
        let cachedValue = this.getValueFromCache();
        this.oldValue = this.setValue(cachedValue !== undefined ? cachedValue : this.value.value);
    }

    saveValue(val) {
        // console.log("saveValue", val)
        // this.saveValueInCache(newVal)
    }

    getKey() {
        let per_channel = this.conf.per_channel || null;
        let per_locale = this.conf.per_locale || null;

        const channelId = this.formStore.conf.channelId;
        const locale = this.formStore.conf.locale;

        return `${per_channel ? channelId : null}.${per_locale ? locale : null}.${this.conf.key}`;
    }

    getValueFromCache() {
        this.checkCache();
        const key = this.getKey();

        if (Object.keys(this.formStore.cache.values).includes(key)) {
            return this.formStore.cache.values[key]
        }
    }

    checkCache() {
        if (!this.formStore.cache.values) {
            this.formStore.cache.values = {}
        }
    }

    saveValueInCache(value) {
        this.checkCache();
        const key = this.getKey();

        this.formStore.cache.values[key] = value;
    }

    clearCache() {
        this.saveValueInCache(undefined)
    }

    @action setValue(value) {
        return new this.ValueStore(value, this.conf, this)
    }

    @computed get isChanged() {
        return this.oldValue !== this.value
    }

    @computed get hasDependence() {
        return this.dependence && (this.dependence.key.length > 0)
    }

    @computed get dependenceKey() {
        if (this.hasDependence) {
            return this.dependence.key
        }

        return null
    }

    @computed get currentDependenceValue() {
        return this.formStore.fieldStores[this.dependenceKey]?.cleanData
    }

    /*
    Возвращает true, если значение зависимого поля соответствует значению в dependency.value

    если dependency.value было не передано - возвращает труд
     */
    @computed get hasProperDependenceValue() {
        let value = this.formStore.fieldStores[this.dependenceKey]?.cleanData
        if (!this.dependence.value) {
            return true
        } else {

            return this.dependenceCompareFunction(value)
        }
    }


    @computed get currentDependenceValueIsNone() {
        if (Number.isInteger(this.currentDependenceValue)) {
            return false
        }
        return !this.currentDependenceValue
    }

    @action removeElement(index) {
        this.value = this.value.filter((el, idx) => {
            return idx !== index
        });
    }

    init() {
    }

    /*
    show - optional argument
     */
    toggleShow(show) {

        // if show in args:
        if (typeof show === 'boolean') {
            this.show = show
            return
        }

        this.show = !this.show
    }

    get cleanData() {
        let cachedValue = this.getValueFromCache();
        return this.value.cleanData
    }

    @action.bound setErrors(errors) {
        const key = this.getKey();
        this.formStore.cache.errors[key] = errors
    }

    @computed get errorMessage() {
        const key = this.getKey();
        return this.formStore.cache.errors[key];
    }

    @action.bound cleanErrors() {
        const key = this.getKey();
        delete this.formStore.cache.errors[key]
    }

}

export default FieldStore;
