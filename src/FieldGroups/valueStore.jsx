import {action, observable} from "mobx";

export default class BaseValueStore {
    @observable value;
    @observable valueError;

    timeoutValidate;

    constructor(value, conf, store) {
        this.value = value || "";
        this.conf = conf;
        this.store = store
    }

    @action.bound valueOnChange(value) {
        this.value = value;
        this.store.saveValueInCache(value)

    }

    @action validate() {
    }

    callValidate() {
        if (this.timeoutValidate) {
            clearTimeout(this.timeoutValidate)
        }

        this.timeoutValidate = setTimeout(() => {
            this.validate()
        }, 500)
    }

    hasError() {
        return !!this.valueError
    }

    get cleanData() {
        return this.value
    }

    equal(valueStore) {
        return this.value === valueStore.value
    }
}
