import DatetimeComponent from "FieldGroups/Datetime/component";
import {action} from "mobx";
import BehaviorFieldStore from "Behavior/behaviorUtils";
import validate from "validate.js";
import BaseValueStore from "FieldGroups/valueStore";


class ValueStore extends BaseValueStore {
    @action validate() {
        if (isNaN(this.value) && this.value !== undefined) return `${utils.getTranslation("ValidationError.invalid")}`;
        if (!this.conf.required && (this.value === "" || this.value === null || this.value === undefined)) return "";
        if (this.conf.required && (this.value === "" || this.value === null || this.value === undefined)) {
            return `^${utils.getTranslation("ValidationError.Required")}`
        }


        const constraints = {
            value: {
                // numericality: {
                //     onlyInteger: !this.conf.decimal_value,
                //
                //     greaterThanOrEqualTo: !is_negative || !this.conf.min_value ? greaterThan : undefined,
                //     lessThanOrEqualTo: (this.conf.max_value !== undefined || this.conf.max_value !== "" || this.conf.max_value !== null) ? lessThanOrEqualTo : undefined,
                //
                //     notGreaterThanOrEqualTo: `^${utils.getTranslation("ValidationError.LessThan")} ${greaterThan}`,
                //     notLessThanOrEqualTo: `^${utils.getTranslation("ValidationError.GreatThan")} ${lessThanOrEqualTo}`,
                //     notInteger: `^${utils.getTranslation("ValidationError.IntegerOnly")}`
                // }
            }
        };

        const result = validate.validate({value: this.value}, constraints);

        this.valueError = result ? result.value.join("<br/> ") : ""
    }
}


export default class DatetimeStore extends BehaviorFieldStore {
    Component = DatetimeComponent;

    init() {
        this.ValueStore = ValueStore;
    }

}