import MultiSelectStore from 'FieldGroups/Select/multiSelect'
import SelectStore from "FieldGroups/Select/simpleSelect";
import TagSelectStore from 'FieldGroups/Select/tagSelect'

export default SelectStore;
export {MultiSelectStore, TagSelectStore}
