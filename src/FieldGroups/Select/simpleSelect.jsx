import SelectComponent from "FieldGroups/Select/component";
import {action, autorun, computed} from "mobx";
import FieldStore from "FieldGroups/store";
import BaseValueStore from "FieldGroups/valueStore";


export default class SelectStore extends FieldStore {
    Component = SelectComponent;
    innerStore

    constructor(...args) {
        super(...args);

        if (this.hasDependence && this.formStore.enableDependence) {
            this.disposeDependencyListener = autorun(() => this.listenDependenceChanges())
        }

    }

    clearValue() {
        delete this.conf.extraParams[this.dependenceKey]

        if (this.innerStore) {
            this.conf.value = ""
            this.conf.extraParams[this.dependenceKey] = null;
            this.innerStore.addValue("", {})
        }
    }

    listenDependenceChanges() {
        if (this.currentDependenceValueIsNone) {
            this.toggleShow(false)
            this.clearValue()
            return
        }

        // clear select
        if (this.innerStore) {
            this.innerStore.addValue("", {})
        }

        // если значение уже было заполнено
        if (this.conf.extraParams[this.dependenceKey] === this.currentDependenceValue
            && this.hasProperDependenceValue) {
            if (this.show === false) {
                this.toggleShow(true)
            }
            return
        }

        this.conf.extraParams[this.dependenceKey] = this.currentDependenceValue
        this.toggleShow(this.hasProperDependenceValue)
    }

    setInnerStore(store) {
        this.innerStore = store
    }

    @computed get isChanged() {
        if (!this.oldValue.value && !this.value.value) {
            return false
        }

        return !this.oldValue.equal(this.value)
    }

    get cleanData() {
        return this.value.cleanData
    }

}

export {BaseValueStore}
