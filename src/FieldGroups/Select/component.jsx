import React from "react";
import Label from "../Label";
import ErrorMessageComponent from "../ErrorMessageComponent";
import {uuid4} from "@kaiju.ui/components/src/utils";
import ESelectComponent from "@kaiju.ui/components/src/Select/ESelectComponent";

export default class SelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.id = uuid4();
        this.conf = this.props.store.conf;
    }

    render() {
        // Функция для поднятия и стора наверх после его иницализации
        const getStore = (fullStore) => {
            this.props.store.setInnerStore(fullStore)
        };

        return (
            <div className="form-group mw-500 relative">
                <Label props={this.props}/>
                <div className="pb-3" style={{position: "relative", marginLeft: "2px", marginRight: "2px"}}>
                    <ESelectComponent
                        conf={{...this.conf, value: this.props.store.value.value}}
                        key={uuid4()}
                        onChange={this.props.store.value.valueOnChange.bind(this.props.store.value)}
                        getStore={getStore}
                    />
                    <ErrorMessageComponent  {...this.props} className="pt-2"/>
                    {this.props.children}
                </div>
            </div>
        )
    }
}