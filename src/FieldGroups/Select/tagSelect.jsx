import SelectStore from "FieldGroups/Select/simpleSelect";
import {MultiValueStore} from 'FieldGroups/Select/multiSelect' 

export default class TagSelectStore extends SelectStore {
    init() {
        this.ValueStore = MultiValueStore;
        this.conf.isMulti = true
        this.conf.isCreatable = true
    }
}