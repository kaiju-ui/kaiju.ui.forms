import React from "react";
import Label from "FieldGroups/Label";
import ReactJson from 'react-json-view'
import './index.scss';
import ErrorMessageComponent from "FieldGroups/ErrorMessageComponent";

export default class JsonComponent extends React.Component {
    render() {
        const readOnly = this.props.store.conf.read_only;
        let jsonProps = {};

        if (!readOnly) {
            jsonProps['onAdd'] = e => this.props.store.value.handleChangeJson(e);
            jsonProps['onEdit'] = e => this.props.store.value.handleChangeJson(e);
            jsonProps['onDelete'] = e => this.props.store.value.handleChangeJson(e);
        }

        return (
            <div className="form-group mw-500 relative pb-3">
                <Label props={this.props}/>
                <ReactJson
                    name={null}
                    src={this.props.store.value.value}
                    {...jsonProps}
                />
                <ErrorMessageComponent  {...this.props} className="pt-2"/>
            </div>
        )
    }
}