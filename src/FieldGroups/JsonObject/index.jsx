import JsonComponent from "FieldGroups/JsonObject/component";
import {action, computed, observable} from "mobx";
import FieldStore from "FieldGroups/store";
import equal from "deep-equal";

class ValueStore {
    @observable value;

    constructor(value, conf, store) {
        this.conf = conf;
        this.store = store;

        if (!value) {
            this.defaultValue = this.conf.behavior === 'list' ? [] : {};
        }

        this.value = value || this.defaultValue;
    }


    isChanged(valueStore) {
        return !equal(this.value, valueStore.value, {strict: true})
    }

    @action handleChangeJson = e => {
        this.value = e.updated_src
    };

    hasError() {
        return !!this.valueError
    }

    get cleanData() {
        return this.value
    }
}


export default class JsonStore extends FieldStore {
    Component = JsonComponent;

    init() {
        this.ValueStore = ValueStore;
    }

    @computed get isChanged() {
        return this.oldValue.isChanged(this.value)
    }

    get cleanData() {
        return this.value.cleanData
    }

}
