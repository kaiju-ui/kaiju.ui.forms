import NumberComponent from "FieldGroups/Number/component";
import {action, observable} from "mobx";
import BehaviorFieldStore from "Behavior/behaviorUtils";
import validate from "validate.js";


class ValueStore {
    @observable value;
    @observable valueError;

    timeoutValidate;

    constructor(value, conf, store) {
        if (!value && !(value === 0.0 || value === 0)){
            value = conf.default || "";
        }

        this.value = value
        this.conf = conf;
        this.store = store
    }

    @action validate() {
        if (isNaN(this.value) && this.value !== undefined) return `${utils.getTranslation("ValidationError.invalid")}`;
        if (!this.conf.required && (this.value === "" || this.value === null || this.value === undefined)) return "";
        if (this.conf.required && (this.value === "" || this.value === null || this.value === undefined)) {
            return `^${utils.getTranslation("ValidationError.Required")}`
        }
        const greaterThan = this.conf.min_value || 0;
        const lessThanOrEqualTo = this.conf.max_value;
        const is_negative = this.conf.is_negative || this.conf.negative_value || false;
        const gte = this.conf.min_value? this.conf.min_value : !is_negative? 0 : undefined;

        const constraints = {
            value: {
                numericality: {
                    onlyInteger: !this.conf.decimal_value,

                    greaterThanOrEqualTo: gte,
                    lessThanOrEqualTo: (this.conf.max_value !== undefined || this.conf.max_value !== "" || this.conf.max_value !== null) ? lessThanOrEqualTo : undefined,

                    notGreaterThanOrEqualTo: `^${utils.getTranslation("ValidationError.LessThan")} ${greaterThan}`,
                    notLessThanOrEqualTo: `^${utils.getTranslation("ValidationError.GreatThan")} ${lessThanOrEqualTo}`,
                    notInteger: `^${utils.getTranslation("ValidationError.IntegerOnly")}`
                }
            }
        };

        const result = validate.validate({value: this.value}, constraints);

        this.valueError = result ? result.value.join("<br/> ") : ""

    }

    callValidate() {
        if (this.timeoutValidate) {
            clearTimeout(this.timeoutValidate)
        }

        this.timeoutValidate = setTimeout(() => {
            this.validate()
        }, 500)
    }

    @action valueOnChange(value) {
        this.value = value ? Number(value) : value;
        this.callValidate()
    }


    get zeros() {
        return [0.0, 0]
    }

    equal(valueStore) {
        // check zero
        let newIsZero = this.zeros.includes(valueStore.value)
        let oldIsZero = this.zeros.includes(this.value)
        if (newIsZero && oldIsZero){
            return true
        } else if ((newIsZero && !oldIsZero) || (!newIsZero && oldIsZero)) {
            return false
        } else if (!this.value && !valueStore.value) {
            return true
        } else {
            return this.value === valueStore.value
        }
    }
    hasError() {
        return !!this.valueError
    }
    get cleanData(){
        return this.value
    }

}


export default class NumberStore extends BehaviorFieldStore {
    Component = NumberComponent;

    init() {
        this.ValueStore = ValueStore;
    }

}


class DecimalStore extends NumberStore {
    constructor(conf, formStore){
        conf.decimal_value = true;
        super(conf, formStore)
    }
}

export {DecimalStore}
