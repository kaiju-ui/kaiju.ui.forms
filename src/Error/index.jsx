import React from "react";
import ReactTooltip from "react-tooltip";

export default class ErrorComponent extends React.Component {
    constructor({id, useTooltip, value}) {
        super();
        this.id = id;
        this.value = value || "";
        this.useTooltip = useTooltip || false;
    }

    render() {
        return (
            <React.Fragment>
                {(this.value && this.value.length > 0) &&
                <React.Fragment>

                    {this.useTooltip &&
                    <ReactTooltip place="right" type="error" effect="solid" id={this.id}>
                        {this.value}
                    </ReactTooltip>
                    }
                    {!this.useTooltip &&
                    <div className="m-form__help text-danger">
                        {this.value}
                    </div>
                    }

                </React.Fragment>
                }
            </React.Fragment>
        )
    }
}