import {action, observable} from "mobx";


const FormCache = {};


class CacheStore {
    @observable errors = {};

    constructor(){
        this.values = {};
    }
    @action.bound setError(key, value){
        this.errors[key] = value
    }

    @action.bound setErrors(errors){
        this.errors = errors
    }
    clear(){
        this.errors = {};
        this.values = {}
    }
}

const getCache = (form) => {
    let cache = FormCache[form.prefix];
    if (!cache){
        cache = new CacheStore()
    }
    FormCache[form.prefix] = cache;
    return cache
};

const deleteCache = (form) => {
    delete FormCache[form.prefix]
};

const clearCache = (form) => {
    FormCache[form.prefix].clear()
};
export default getCache
export {deleteCache, FormCache, clearCache}