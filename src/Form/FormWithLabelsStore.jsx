import {FormStore} from "Form/FormStore";
import {action, computed, observable} from "mobx";
import Axios from "axios";
import {notifySuccess} from "@kaiju.ui/components/src/notifications";

/**
 * Стор для осуществления работы с двумя формами: main  и labels.
 * Используется для страниц редактирования и добавления
 */
class FormWithLabelsStore {
    @observable isFetching = false;

    /**
     *
     * @param conf основной конфиг
     * @var labelKey - ключ для доступа к fieldStores, чаще всего "id" или "key"
     * @var formStore
     * @var labelsFormStore
     * @var systemLocale
     * @var successCallback  - callback после сохранения формы
     */
    constructor(conf) {
        this.conf = conf;
        this.labelKey = conf.labelKey;
        this.successCallback = this.conf.successCallback;
        this.systemLocale = this.conf.systemLocale;
        this.conf.formStore.clearCache = true;
        this.conf.labelsFormStore.clearCache = true;
        this.formStore = new FormStore(this.conf.formStore, this.conf.extraParams || {});
        this.labelsFormStore = new FormStore(this.conf.labelsFormStore);
        this.changesCallback = this.conf.changesCallback;

        window.onbeforeunload = this.beforeReload.bind(this);
    }

    beforeReload(event) {
        return this.isChanged ? utils.getTranslation("Message.lost_changes") : null
    }

    @action
    reloadForms() {
        this.labelsFormStore.actions.reload();
        this.formStore.actions.reload();
    }

    setErrors(response) {
        let {error} = response.data;

        if (error && error.data.type === "ModelValidationException") {
            this.formStore.setErrors(error.data.fields)
        } else {
            utils.handleNotifyExceptions(response)
        }
    }

    /**
     *  Проверка внесения изменений для двух форм
     * @returns {boolean}
     */
    @computed
    get isChanged() {
        let isChanged = this.formStore.changes > 0 || this.labelsFormStore.changes > 0;

        if (this.changesCallback) {
            this.changesCallback(isChanged)
        }

        return isChanged
    }

    /**
     * Смотрит произошел ли init двух форм
     * @returns {boolean}
     */
    @computed get dataInitialized() {
        return this.formStore.actions.dataInitialized
            && this.labelsFormStore.actions.dataInitialized
    }

    /**
     * Лейбл для хедера
     *
     * @returns {string|""} - актуальный лейбл на момент редактирования
     */
    @computed get label() {
        let locale = this.systemLocale;

        if (this.labelsFormStore.fieldStores &&
            this.labelsFormStore.fieldStores[locale] &&
            this.labelsFormStore.fieldStores[locale].value.value
        ) {
            return this.labelsFormStore.fieldStores[locale].value.value
        }

        if (this.formStore.fieldStores &&
            this.formStore.fieldStores[this.labelKey] &&
            this.formStore.fieldStores[this.labelKey].value.value) {
            return `[${this.formStore.fieldStores[this.labelKey].value.value}]`
        }

        return ""
    }

    saveCallback() {
        const isValid = true; // TODO:

        if (!isValid) {
            // TODO:
        }

        let params = {
            ...(this.conf.requests.save.params || {}),
            ...this.formStore.cleanData
        };


        params["labels"] = this.labelsFormStore.cleanData;

        this.isFetching = true;

        Axios.post('/public/rpc', {
                method: this.conf.requests.save.method,
                params: params
            }
        ).then((response) => {
            const result = response.data.result;

            if (result) {
                notifySuccess();

                if (this.successCallback) {
                    this.successCallback(result);
                }

                this.reloadForms();
            } else {
                this.setErrors(response)
            }

        })
            .catch((response) => utils.handleNotifyExceptions(response))
            .finally(() => this.isFetching = false)
    }
}

export default FormWithLabelsStore