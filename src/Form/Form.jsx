import React from 'react';
import {observer, Provider} from "mobx-react";
import {uuid4} from "@kaiju.ui/components/src/utils";
import "./Form.scss"

@observer
class Form extends React.Component {
    changesMessageHeight = 40;

    constructor(props) {
        super(props);

        this.store = props.store;

        this.labelStyle = {
            position: "sticky",
            top: (this.props.top || 0) + (this.store.hasHeader ? this.changesMessageHeight : 0) + "px",
            zIndex: 1,
            backgroundColor: "white"
        };

        this.headerStyle = {
            position: "sticky",
            top: this.props.top || "0px",
            zIndex: 1,
            backgroundColor: "white",
            padding: "10px 0",
            height: `${this.changesMessageHeight}px`
        }
    }

    headerMessage = observer(() => {
        return (
            <React.Fragment>
                {
                    this.store.changes > 0 &&
                    <React.Fragment>
                        <b>{utils.getTranslation("Message.count")}: {this.store.changes}</b>
                        {this.store.all &&
                        <a className="pl-2 pointer" onClick={e => this.store.actions.showChanged()}>показать</a>
                        }
                        {!this.store.all &&
                        <a className="pl-2 pointer" onClick={e => this.store.actions.showAll()}>показать все</a>
                        }
                    </React.Fragment>
                }
            </React.Fragment>
        )
    });

    renderField = observer(({fieldStore}) => {
        return (
            <React.Fragment>
                {fieldStore.show &&
                <fieldStore.Component
                    key={fieldStore.uuid}
                    store={fieldStore}
                />}
            </React.Fragment>
        )
    });

    componentWillUnmount() {
        // clear dependency listeners on form umnount
        Object.keys(this.props.store.fieldStores).map(k => {
            let fs = this.store.fieldStores[k]
            if (fs.hasDependence && this.store.enableDependence) {
                fs.disposeDependencyListener()
            }
        });
    }

    render() {
        return (
            <Provider formStore={this.store}>
                <div className={`kaiju-form__form ${this.props.className || ""}`} key={this.store.key}>

                    {
                        this.store.hasHeader &&
                        <div className="border-bottom mb-3" style={this.headerStyle}>
                            <div className="flex">
                                <div className="mr-auto">
                                    <this.headerMessage/>
                                </div>
                            </div>
                        </div>
                    }
                    {
                        !this.store.isFetching && this.store.groups.map((group) => {
                            return (
                                group.showed &&
                                <React.Fragment key={group.uuid}>
                                    {!this.store.disableGroup &&
                                    <div className={`form-group pb-2 border-bottom mw-500 kaiju-form__group`}
                                         style={this.labelStyle}>
                                        <span className="m--icon-font-size-lg1 m--font-bolder">
                                            {group.label}
                                        </span>
                                    </div>
                                    }

                                    {
                                        group.fields.map((fieldStore) => <this.renderField key={uuid4()}
                                                                                           fieldStore={fieldStore}/>)
                                    }
                                </React.Fragment>
                            )
                        })
                    }
                    {this.props.children &&
                    this.props.children
                    }
                </div>
            </Provider>

        )
    }

}

export default Form;